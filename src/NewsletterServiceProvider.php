<?php


namespace Azizyus\Newsletter;


use Illuminate\Support\ServiceProvider;

class NewsletterServiceProvider extends ServiceProvider
{

    public function boot()
    {

        $this->loadMigrationsFrom(__DIR__."/Migrations");

    }

}