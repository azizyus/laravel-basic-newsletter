<?php

namespace Azizyus\Newsletter\Model;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $table = 'newsletters';

    protected $fillable = ['email','created_at','updated_at'];

}