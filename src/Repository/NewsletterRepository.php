<?php

namespace Azizyus\Newsletter\Repository;

use Illuminate\Support\Collection;
use Azizyus\Newsletter\Model\Newsletter;

class NewsletterRepository
{

    public function baseQuery()
    {
        return Newsletter::query();
    }

    public function updateOrInsert(Collection $collection)
    {
        $found = $this->getByEmail($collection->get("email"));

        if(!$found)
        {
            $found =new Newsletter();
            $found->email = $collection->get("email");
            $found->deleteHash = md5(uniqid());
        }
        $found->save();
    }

    public function getByEmail($email)
    {
        return $this->baseQuery()->where("email",$email)->first();
    }

    public function getAll()
    {
        return $this->baseQuery()->get();
    }

    public function getById($id)
    {
        return $this->baseQuery()->find($id);
    }

}